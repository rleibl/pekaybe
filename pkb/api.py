
import json
import pkb.knowledgebase as kb


def search(request):
    q = request.POST.getone('q')
    q = q.split(' ')

    articles = kb.get_instance().search(q)

    ret = {
        'articles': articles,
        'tags': [],
        'search': q
    }
    return ret
