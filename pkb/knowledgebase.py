
import string

_files = ['main.kb']
_kb = None 

def init():
    global _kb
    _kb = Knowledgebase('main.kb')

def get_instance():
    if not _kb:
        init()

    return _kb
    

class Knowledgebase():
    def __init__(self, kb):
        f = open(kb, 'r')
        content = f.readlines()

        kb = []

        item = self._empty_entry()
        for line in content:
            if line.startswith('----------'):
                kb.append(item)
                # reset item
                item = self._empty_entry()
                continue
            if line.startswith('+tags:'):
                l = line.lstrip('+tags:').rstrip('\n')
                t = l.split(',')
                item["tags"].extend( [ i.replace(' ', '') for i in t] )
                continue

            line = line.replace('<', '&lt;')
            line = line.replace('>', '&gt;')
            item['text'] += line

        self.kb = kb

        for item in kb:
            print('--------------------------')
            print('tags', item['tags'])
            print(item['text'])


    def _empty_entry(self):
        return {
                  "text": "",
                  "tags": []
               }
            


    def search(self, term):
        # search term is a list of (possibly) space separated
        # search items. Make one list of search items.
        t = " ".join(term)
        term = t.split(' ')

        # search set is the whole kb. After each search word, 
        # only the ones matching are left into search set
        search_set = self.kb
        result = []

        for search_term in term:
            result = []
            print("searchterm: {}".format(search_term))
            print("search_set: {}".format(search_set))
            for item in search_set:
                for tag in item['tags']:
                    print("checking tag {} == {}".format(search_term, tag))
                    if tag.find(search_term) != -1:
                        print("found match")
                        result.append(item)
            if not result:
                return []

            search_set = result

        # result contains the whole dict {'text': '...', 'tags': []}
        r = [ i['text'] for i in result ]
        print("search result: ", r)

        return r

