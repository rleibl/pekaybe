
import pyramid.httpexceptions as exc
from pyramid.response import Response
from mako.template import Template
from mako.lookup import TemplateLookup
import pkb.knowledgebase as kb
from pkb.session import Session
import pkb.session as pkbses

lookup = TemplateLookup(directories=['/Users/ovid/git/pekaybe/pkb/templates', 'pkb/templates/'])


# ---------------------------------------------------------
def login(request):
    error = False

    if request.method == 'POST':
        u = request.POST.getone('username')
        p = request.POST.getone('password')

        if(u == 'foo' and p == 'pppp'):
            try:
                s = Session(u, request.cookies['sessionid']) # FIXME hardcoded name
            except KeyError:
                # no sessionid. Cookies disabled or not from login page
                # redirect to login page
                raise exc.HTTPFound(request.route_url("login"))

            raise exc.HTTPFound(request.route_url("pkb"))

        error = True

    t = lookup.get_template("login.html")
    r = Response(t.render(error=error))
    r.set_cookie('sessionid', value=pkbses.new_cookie()) # FIXME hardcoded name
    return r

# ---------------------------------------------------------
def pkb(request):
    ses = pkbses.get_session(request)

    articles = []
    searches = request.GET.getall('search');
    if searches:
        print("searching for ", searches)
        kbase = kb.get_instance()
        articles = kbase.search(searches)

    print("articles", articles)

    t = lookup.get_template("pkb.html")
    s = ", ".join(searches)
    return Response(t.render(searchterm=s, results=articles, session=ses))

# ---------------------------------------------------------
def example(request):
    t = lookup.get_template("example.html")
    return Response(t.render())

