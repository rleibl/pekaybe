from wsgiref.simple_server import make_server
from pyramid.config import Configurator
import pkb.views
import pkb.api
import pkb.knowledgebase as kb

if __name__ == '__main__':
    with Configurator() as config:

        # maybe redirect '/' to '/pkb'
        config.add_route('pkb', '/')
        config.add_view(pkb.views.pkb, route_name='pkb')

        config.add_route('example', '/example')
        config.add_view(pkb.views.example, route_name='example')

        config.add_static_view('static', 'pkb/static')

        config.add_route('login', '/login')
        config.add_view(pkb.views.login, route_name='login')

        #
        # API routing
        #
        config.add_route('api_search', '/api/1.0/pkbsearch')
        config.add_view( pkb.api.search,
                         route_name='api_search',
                         renderer='json')

        kb.init()


        app = config.make_wsgi_app()

    print(" server at %s:%s" % ("localhost", "8080"))
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever()
